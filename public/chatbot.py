"""Chatbot-Modul"""
# -*- coding: utf-8 -*-
import random
from pyscript import document

class ChatBot:
    """Klasse für den Chatbot"""
    def __init__(self, inputname, outputname):
        self.passende_antworten = { # Dictionary mit passenden FAQ-Antworten
            "blinkt" : "Starten Sie das Gerät einmal neu. Die Anleitung finden Sie unter *Link*",
			"verschmiert" : "Nutzen Sie die Reinigungsfunktion des Gerätes. Die Anleitung finden Sie unter *Link*",
            "nichts" : "Prüfen Sie, ob alle Kabel richtig angeschlossen sind. Die Anleitung finden Sie unter *Link*",
            "bild" : "Starten Sie das Gerät einmal neu. Die Anleitung finden Sie unter *Link*",
            "agb" : "Unsere AGB finden Sie unter *Link*",
			"kaufen" : "Bitte wenden Sie sich an unseren Vertrieb: *E-Mail*/*Telefon*",
            "rechnung" : "Bitte wenden Sie sich an unsere Buchhaltung: *E-Mail*/*Telefon*",
            "passwort" : "Bitte wenden Sie sich an unsere Hotline (*E-Mail*/*Telefon*) oder erstellen Sie ein Ticket."
        }
        self.smalltalk_antworten = { # Dictionary mit Smalltalk-Antworten
            "hallo" : "Hallo!",
			"hi" : "Hi!",
            "moin" : "Moin!",
            "danke" : "Gern geschehen!",
			"lol" : "Ha ha ha!",
            "wetter" : "In meinem Rechnenzentrum ist es warm und trocken."
        }
        self.random_antworten = [ # Liste mit zufälligen Antworten
            "Aha...",
            "Damit kann ich leider nicht helfen...",
            "Könnten Sie das präzisieren?"
		]
        self.nachrichten = { # Dictionary mit speziellen Antworten
            "willkommen": """Willkommen beim Chatbot der Solutions IT!
                Wie kann ich Ihnen behilfich sein?""",
            "verabschieden": "Einen schönen Tag wünsche ich Ihnen. Bis zum nächsten Mal!",
            "ticket_hinweis": """Ich hoffe ich konnte behilflich sein. 
                Ansonsten antworten Sie mit "Ticket erstellen", um mit meiner Hilfe ein Support-Ticket zu erstellen."""
        }
        self.ticket_anworten = [ # Liste mit Antworten die bei der Ticketerstellung nacheinander abgefragt werden
            """Okay, erstellen wir ein Ticket.
                Bitte antworten Sie immer nur genau auf meine Frage.""",
            "Wie ist Ihre Kundennummer?",
            "Wie lautet die Seriennummer des defekten Gerätes",
            "Bitte geben Sie eine genaue Problembeschreibung an.",
            "Das Ticket wurde erfolgreich erstellt!"
		]
        self.passende_antwort_gegeben = False
        self.smalltalk_antwort_gegeben = False
        self.ticket_step = -1
        self.input_keywords = []
        self.input_text = ""
        self.ticket_text = []
        self.input_element = document.querySelector(inputname)
        self.output_element = document.querySelector(outputname)

    def create_element(self, typ, text):
        """Element/Chatbubble in Chatverlauf einfügen"""
        element = document.createElement('div')
        bubble_head = document.createElement('div')
        bubble_head.className = "bubble-head"
        if typ == "kunde":
            element.className = 'kunde'
            bubble_head.innerText = "👤 Sie\n"
        if typ == "bot":
            element.className = 'bot'
            bubble_head.innerText = "🤖 Chatbot\n"
        bubble_foot = document.createElement('div')
        bubble_foot.className = "bubble-foot"
        bubble_foot.innerText = text
        element.appendChild(bubble_head)  
        element.appendChild(bubble_foot) # Oberen und unteren Teil der Chat-Bubble in einem Element zusammenfügen
        self.output_element.insertBefore(element, self.output_element.firstChild)

    def nachricht(self, typ):
        """Willkommensnachricht & Verabschiedung ausgeben"""
        text = self.nachrichten[typ]
        self.create_element("bot", text)

    def random_antwort(self):
        """Zufällige Antwort ausgeben"""
        text = random.choice(self.random_antworten)
        self.create_element("bot", text)

    def erstelle_keywords(self, text):
        """User Input säubern und in eine Liste aus Wörtern umwandeln"""
        return text.lower().strip().strip('.').strip(',').strip('!').strip('?').split()

    def passende_antwort(self):
        """Zu einem Keyword passende Support-Antwort ausgeben"""
        for keyword, antwort in self.passende_antworten.items():
            if keyword in self.input_keywords:
                self.create_element("bot", antwort)
                self.passende_antwort_gegeben = True

    def smalltalk_antwort(self):
        """Zu einem Keyword passende Smalltalk-Antwort ausgeben"""
        for keyword, antwort in self.smalltalk_antworten.items():
            if keyword in self.input_keywords:
                self.create_element("bot", antwort)
                self.smalltalk_antwort_gegeben = True

    def ticket_versenden(self):
        """Ticket im Chat anzeigen und versenden"""
        # Versand ist nicht implementiert und Ticketnummer wird zufällig erstellt
        self.create_element("bot",f"""Ihre Ticketnummer lautet #{random.randint(1111,9999)}.
                > Kundennummer: {self.ticket_text[0]} 
                > Seriennummer: {self.ticket_text[1]} 
                > Problembeschreibung: {self.ticket_text[2]}""")
        self.ticket_text = []

    def ticket_antwort(self):
        """Fragen für die Ticket-Erstellung"""
        if len(self.ticket_anworten) > self.ticket_step > -1 :
            if self.ticket_step > 1: # Ab dem zweiten Schritt Kundenantworten sammeln
                self.ticket_text.append(self.input_text)
            self.create_element("bot",self.ticket_anworten[self.ticket_step])
            self.ticket_step += 1
        if self.ticket_step >= len(self.ticket_anworten) :
            self.ticket_versenden() # Wenn alle Fragen durchgegangen wurden, Ticket "versenden"
            self.ticket_step = -1   # und Ticketerstellung zurücksetzen

    def chatten_html(self):
        """Hauptfunktion zum Chatten"""
        if self.input_element.value not in ("","\n"): # Nachricht nur senden wenn Eingabefeld nicht leer
            self.input_text = self.input_element.value
            self.input_element.value = ""
            self.create_element("kunde", self.input_text)  # Chat-Bubble erstellen, mit dem was der Kunde gesendet hat
            self.passende_antwort_gegeben = False
            self.smalltalk_antwort_gegeben = False
            self.input_keywords = self.erstelle_keywords(self.input_text)
            if self.input_text.lower() == "ticket erstellen":
                self.ticket_step = 0
                self.ticket_antwort()
            if set(self.input_keywords) & set(("tschüss", "bye", "wiedersehen", "ciao")):
                self.nachricht("verabschieden")
            else:                                 # Nacheinander die verschiedenen Antwortmöglichkeiten durchgehen
                if self.ticket_step == -1:
                    self.passende_antwort()
                    if self.passende_antwort_gegeben is False:
                        self.smalltalk_antwort()
                        if self.smalltalk_antwort_gegeben is False:
                            self.random_antwort()
                    else: self.nachricht("ticket_hinweis")
                self.ticket_antwort()
