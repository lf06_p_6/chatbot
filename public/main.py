"""Chatbot-Modul"""
# -*- coding: utf-8 -*-
from pyscript import when
from chatbot import ChatBot

if __name__ == "__main__":
    OUTPUT_NAME = "#chat-messages"      # Namen der HTML/CSS-Elemente festlegen
    INPUT_NAME = "#input"
    BUTTON_NAME = "#chat_button"
    chatbot = ChatBot(INPUT_NAME, OUTPUT_NAME)      # Chatbot-Objekt erstellen
    chatbot.nachricht("willkommen")

    @when("click", BUTTON_NAME)
    def chatten_b(event):
        """Funktion die beim Drücken des Buttons 'Senden' ausgeführt wird"""
        chatbot.chatten_html()

    @when("keypress", INPUT_NAME)
    def chatten_i(event):
        """Funktion die beim Absenden per 'Enter' ausgeführt wird"""
        if event.key == "Enter":
            chatbot.chatten_html()
            event.stopPropagation()     # Bei Enter nur Eingabe ansenden,
            event.preventDefault()      # keine neue Zeile erstellen
