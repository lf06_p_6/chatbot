"""Chatbot-Modul"""
# -*- coding: utf-8 -*-
import random

class ChatBot:
    """Klasse für den Chatbot"""
    def __init__(self):
        self.passende_antworten = {
            "hallo": "Hallo!",
			"problem": "Bitte eröffnen Sie ein Support-Ticket unter *Link*",
            "agb" : "Unsere AGB finden Sie unter *Link*",
			"kaufen": "Bitte wenden Sie sich an unseren Einkauf: *E-Mail*/*Telefon*"
        }
        self.random_antworten = [
            "Aha...",
            "Damit kann ich leider nicht helfen...",
            "Könnten Sie das präzisieren?"
		]
        self.passende_antwort_gegeben = False

    def print_willkommen(self):
        """Willkommensnachricht ausgeben"""
        print("Willkommen beim Chatbot der Solutions IT")
        print("Wie kann ich ihnen behilfich sein?")
        print("Zum beenden einfach 'tschüss' eintippen")
        print("")

    def print_verabschieden(self):
        """Verabschieden"""
        print("Einen schönen Tag wünsche ich Ihnen. Bis zum nächsten Mal!")

    def print_random_antwort(self):
        """Zufällige Antwort ausgeben"""
        print(random.choice(self.random_antworten))

    def print_passende_antwort(self, message):
        """Zu einem Keyword passende Antwort ausgeben"""
        message = message.lower().strip('.').strip(',').strip('!').strip('?').split()
        for keyword, antwort in self.passende_antworten.items():
            if keyword in message:
                print(antwort)
                self.passende_antwort_gegeben = True

    def chatten_console(self):
        """Hauptfunktion zum Chatten in der Konsole"""
        self.print_willkommen()
        while True:
            user_input = input("Ihre Frage: ")
            self.passende_antwort_gegeben = False
            if user_input.lower() == "tschüss":
                break
            self.print_passende_antwort(user_input)
            if self.passende_antwort_gegeben is False:
                self.print_random_antwort()
            print("")
        self.print_verabschieden()

if __name__ == "__main__":
    chatbot = ChatBot()
    chatbot.chatten_console()
