# Chatbot

## Beschreibung
Chatbot für Lernfeld 6 der Gruppe P6.

Nutzt PyScript um den in Python geschriebenen Chatbot auf einer Website darzustellen.

## Dateien/Module
".gitlab-ci.yml" und "Gemfile" werden für GitLab-Pages benötigt.

Der Chatbot liegt im Ordner public.

"index.html" & "chat-style.css"	beschreiben die Website. "pyscript.toml" wird für PyScript benötigt. "main.py" wird von der Website aus gestartet und nutzt das Modul "chatbot.py", in der die Klasse ChatBot programmiert ist.